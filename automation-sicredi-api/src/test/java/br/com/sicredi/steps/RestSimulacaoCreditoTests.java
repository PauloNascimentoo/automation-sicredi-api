package br.com.sicredi.steps;

import com.github.javafaker.Faker;

import br.com.sicredi.base.BaseSetup;

import org.testng.annotations.Test;
import java.util.HashMap;
import java.util.Map;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import java.util.Random;

public class RestSimulacaoCreditoTests extends BaseSetup {

	Faker faker = new Faker();
	Random random = new Random();

	String nome;
	int Cpf;
	String email;
	int valor;
	int parcelas;
	boolean seguro;

	private String NUMERO_CPF;

	@Test
	public void consultaCpfSemRestricao() {
		given().pathParam("cpf", "01837218102").when().get("/api/v1/restricoes/{cpf}").then().statusCode(204);
	}

	@Test
	public void consultaCpfComRestricao() {
		given().pathParam("cpf", "97093236014").get("/api/v1/restricoes/{cpf}").then().statusCode(200).and()
				.assertThat().body("mensagem", equalTo("O CPF 97093236014 tem problema")).log().all();
	}

	@Test
	public void consultaCpfNaoInformado() {
		given().get("/api/v1/restricoes/").then().statusCode(404).and().assertThat().body("status", equalTo(404))
				.body("error", equalTo("Not Found")).body("message", equalTo("No message available"))
				.body("path", equalTo("/api/v1/restricoes/")).log().all();
	}

	@Test
	public void consultaMetodoInvalido() {
		given().pathParam("cpf", "97093236014").post("/api/v1/restricoes/{cpf}").then().statusCode(405).log().all();
	}

	@Test
	public void inseriSimulacaoTrue() {

		Map<String, String> InsereSimulacao = new HashMap<>();

		InsereSimulacao.put("nome", nome = faker.name().fullName());
		InsereSimulacao.put("cpf", String.valueOf(Cpf = random.nextInt(1000000000) * 1 + 3));
		InsereSimulacao.put("email", email = faker.bothify("????##@gmail.com"));
		InsereSimulacao.put("valor", String.valueOf(faker.random().nextInt(10) + 1));
		InsereSimulacao.put("parcelas", String.valueOf(faker.random().nextInt(2) + 1));
		InsereSimulacao.put("seguro", "true");

		NUMERO_CPF = given().contentType("application/json").body(InsereSimulacao).when().post("/api/v1/simulacoes")
				.then().statusCode(201).log().all().extract().path("cpf");

	}

	@Test
	public void inseriSimulacaoFalse() {

		Map<String, String> InsereSimulacao = new HashMap<>();

		InsereSimulacao.put("nome", nome = faker.name().fullName());
		InsereSimulacao.put("cpf", String.valueOf(Cpf = random.nextInt(1000000000) * 1 + 3));
		InsereSimulacao.put("email", email = faker.bothify("????##@gmail.com"));
		InsereSimulacao.put("valor", String.valueOf(faker.random().nextInt(10) + 1));
		InsereSimulacao.put("parcelas", String.valueOf(faker.random().nextInt(2) + 1));
		InsereSimulacao.put("seguro", "false");

		NUMERO_CPF = given().contentType("application/json").body(InsereSimulacao).when().post("/api/v1/simulacoes")
				.then().statusCode(201).log().all().extract().path("cpf");

	}

	@Test
	public void inseriSimulacaoCpfDuplicado() {

		Map<String, String> InsereSimulacao = new HashMap<>();

		InsereSimulacao.put("nome", "Paulo Nascimento");
		InsereSimulacao.put("cpf", "97093233608");
		InsereSimulacao.put("email", "teste@gmail.com");
		InsereSimulacao.put("valor", "25");
		InsereSimulacao.put("parcelas", "5");
		InsereSimulacao.put("seguro", "true");

		given().contentType("application/json").body(InsereSimulacao).when().post("/api/v1/simulacoes").then()
				.statusCode(400).assertThat().body("mensagem", equalTo("CPF duplicado")).log().all();
	}

	@Test
	public void inseriSimulacaoSemInformarCampoNome() {

		Map<String, String> InsereSimulacao = new HashMap<>();

		InsereSimulacao.put("cpf", "01837218102");
		InsereSimulacao.put("email", "teste@gmail.com");
		InsereSimulacao.put("valor", "25");
		InsereSimulacao.put("parcelas", "5");
		InsereSimulacao.put("seguro", "true");

		given().contentType("application/json").body(InsereSimulacao).when().post("/api/v1/simulacoes").then()
				.statusCode(400).assertThat().body("erros.nome", equalTo("Nome não pode ser vazio")).log().all();

	}

	@Test
	public void inseriSimulacaoCpfNaoInformado() {

		Map<String, String> InsereSimulacao = new HashMap<>();

		InsereSimulacao.put("nome", "Paulo Nascimento");
		InsereSimulacao.put("email", "teste@gmail.com");
		InsereSimulacao.put("valor", "25");
		InsereSimulacao.put("parcelas", "5");
		InsereSimulacao.put("seguro", "true");

		given().contentType("application/json").body(InsereSimulacao).when().post("/api/v1/simulacoes").then()
				.statusCode(400).assertThat().body("erros.cpf", equalTo("CPF não pode ser vazio")).log().all();

	}

	@Test
	public void inseriSimulacaoEmailNaoInformado() {

		Map<String, String> InsereSimulacao = new HashMap<>();

		InsereSimulacao.put("nome", "Paulo Nascimento");
		InsereSimulacao.put("cpf", "05307431082");
		InsereSimulacao.put("email", "");
		InsereSimulacao.put("valor", "25");
		InsereSimulacao.put("parcelas", "5");
		InsereSimulacao.put("seguro", "true");

		given().contentType("application/json").body(InsereSimulacao).when().post("/api/v1/simulacoes").then()
				.statusCode(400).assertThat().body("erros.email", equalTo("E-mail deve ser um e-mail válido")).log()
				.all();

	}

	@Test
	public void inseriSimulacaoEmailInvalido() {

		Map<String, String> InsereSimulacao = new HashMap<>();

		InsereSimulacao.put("nome", "Paulo Nascimento");
		InsereSimulacao.put("cpf", "05307431082");
		InsereSimulacao.put("email", "teste");
		InsereSimulacao.put("valor", "25");
		InsereSimulacao.put("parcelas", "5");
		InsereSimulacao.put("seguro", "true");

		given().contentType("application/json").body(InsereSimulacao).when().post("/api/v1/simulacoes").then()
				.statusCode(400).assertThat().body("erros.email", equalTo("não é um endereço de e-mail")).log().all();

	}

	@Test
	public void inseriSimulacaoValorNaoInformado() {

		Map<String, String> InsereSimulacao = new HashMap<>();

		InsereSimulacao.put("nome", "Paulo Nascimento");
		InsereSimulacao.put("cpf", "05307431082");
		InsereSimulacao.put("email", "teste@teste.com");
		InsereSimulacao.put("parcelas", "5");
		InsereSimulacao.put("seguro", "true");

		given().contentType("application/json").body(InsereSimulacao).when().post("/api/v1/simulacoes").then()
				.statusCode(400).assertThat().body("erros.valor", equalTo("Valor não pode ser vazio")).log().all();

	}

	@Test
	public void inseriSimulacaoParcelaMenorQueDois() {

		Map<String, String> InsereSimulacao = new HashMap<>();

		InsereSimulacao.put("nome", "Paulo Nascimento");
		InsereSimulacao.put("cpf", "05307431082");
		InsereSimulacao.put("email", "teste@teste.com");
		InsereSimulacao.put("valor", "5");
		InsereSimulacao.put("parcelas", "1");
		InsereSimulacao.put("seguro", "true");

		given().contentType("application/json").body(InsereSimulacao).when().post("/api/v1/simulacoes").then()
				.statusCode(400).assertThat().body("erros.parcelas", equalTo("Parcelas deve ser igual ou maior que 2"))
				.log().all();

	}

	//// metodo Put

	@Test
	public void editaSimulacaoTrue() {

		Map<String, String> EditaSimulacao = new HashMap<>();

		EditaSimulacao.put("nome", nome = faker.name().fullName());
		EditaSimulacao.put("cpf", String.valueOf(Cpf = random.nextInt(1000000000) * 1 + 3));
		EditaSimulacao.put("email", email = faker.bothify("????##@gmail.com"));
		EditaSimulacao.put("valor", String.valueOf(faker.random().nextInt(10) + 1));
		EditaSimulacao.put("parcelas", String.valueOf(faker.random().nextInt(2) + 1));
		EditaSimulacao.put("seguro", "true");

		given().contentType("application/json").body(EditaSimulacao).when().put("/api/v1/simulacoes/{NUMERO_CPF}")
				.then().statusCode(200).log().all();

	}

	@Test
	public void editaSimulacaoFalse() {

		Map<String, String> EditaSimulacao = new HashMap<>();

		EditaSimulacao.put("nome", nome = faker.name().fullName());
		EditaSimulacao.put("cpf", String.valueOf(Cpf = random.nextInt(1000000000) * 1 + 3));
		EditaSimulacao.put("email", email = faker.bothify("????##@gmail.com"));
		EditaSimulacao.put("valor", String.valueOf(faker.random().nextInt(10) + 1));
		EditaSimulacao.put("parcelas", String.valueOf(faker.random().nextInt(2) + 1));
		EditaSimulacao.put("seguro", "false");

		given().contentType("application/json").body(EditaSimulacao).when().put("/api/v1/simulacoes/{NUMERO_CPF}")
				.then().statusCode(200).log().all();

	}

	@Test
	public void editaSimulacaoCpfDuplicado() {

		Map<String, String> EditaSimulacao = new HashMap<>();

		EditaSimulacao.put("nome", "Paulo Nascimento");
		EditaSimulacao.put("cpf", "97093233608");
		EditaSimulacao.put("email", "teste@gmail.com");
		EditaSimulacao.put("valor", "25");
		EditaSimulacao.put("parcelas", "5");
		EditaSimulacao.put("seguro", "true");

		given().contentType("application/json").body(EditaSimulacao).when().put("/api/v1/simulacoes/{NUMERO_CPF}")
				.then().statusCode(400).assertThat().body("mensagem", equalTo("CPF duplicado")).log().all();
	}

	@Test
	public void editaSimulacaoSemInformarCampoNome() {

		Map<String, String> EditaSimulacao = new HashMap<>();

		EditaSimulacao.put("cpf", "01837218102");
		EditaSimulacao.put("email", "teste@gmail.com");
		EditaSimulacao.put("valor", "25");
		EditaSimulacao.put("parcelas", "5");
		EditaSimulacao.put("seguro", "true");

		given().contentType("application/json").body(EditaSimulacao).when().put("/api/v1/simulacoes/{NUMERO_CPF}")
				.then().statusCode(400).assertThat().body("erros.nome", equalTo("Nome não pode ser vazio")).log().all();

	}

	@Test
	public void editaSimulacaoCpfNaoInformado() {

		Map<String, String> EditaSimulacao = new HashMap<>();

		EditaSimulacao.put("nome", "Paulo Nascimento");
		EditaSimulacao.put("email", "teste@gmail.com");
		EditaSimulacao.put("valor", "25");
		EditaSimulacao.put("parcelas", "5");
		EditaSimulacao.put("seguro", "true");

		given().contentType("application/json").body(EditaSimulacao).when().put("/api/v1/simulacoes/{NUMERO_CPF}")
				.then().statusCode(400).assertThat().body("erros.cpf", equalTo("CPF não pode ser vazio")).log().all();

	}

	@Test
	public void editaSimulacaoEmailNaoInformado() {

		Map<String, String> EditaSimulacao = new HashMap<>();

		EditaSimulacao.put("nome", "Paulo Nascimento");
		EditaSimulacao.put("cpf", "05307431082");
		EditaSimulacao.put("email", "");
		EditaSimulacao.put("valor", "25");
		EditaSimulacao.put("parcelas", "5");
		EditaSimulacao.put("seguro", "true");

		given().contentType("application/json").body(EditaSimulacao).when().put("/api/v1/simulacoes/{NUMERO_CPF}")
				.then().statusCode(400).assertThat().body("erros.email", equalTo("E-mail deve ser um e-mail válido"))
				.log().all();

	}

	@Test
	public void editaSimulacaoEmailInvalido() {

		Map<String, String> EditaSimulacao = new HashMap<>();

		EditaSimulacao.put("nome", "Paulo Nascimento");
		EditaSimulacao.put("cpf", "05307431082");
		EditaSimulacao.put("email", "teste");
		EditaSimulacao.put("valor", "25");
		EditaSimulacao.put("parcelas", "5");
		EditaSimulacao.put("seguro", "true");

		given().contentType("application/json").body(EditaSimulacao).when().put("/api/v1/simulacoes/{NUMERO_CPF}")
				.then().statusCode(400).assertThat().body("erros.email", equalTo("não é um endereço de e-mail")).log()
				.all();

	}

	@Test
	public void editaSimulacaoValorNaoInformado() {

		Map<String, String> EditaSimulacao = new HashMap<>();

		EditaSimulacao.put("nome", "Paulo Nascimento");
		EditaSimulacao.put("cpf", "05307431082");
		EditaSimulacao.put("email", "teste@teste.com");
		EditaSimulacao.put("parcelas", "5");
		EditaSimulacao.put("seguro", "true");

		given().contentType("application/json").body(EditaSimulacao).when().put("/api/v1/simulacoes/{NUMERO_CPF}")
				.then().statusCode(400).assertThat().body("erros.valor", equalTo("Valor não pode ser vazio")).log()
				.all();

	}

	@Test
	public void editaSimulacaoParcelaMenorQueDois() {

		Map<String, String> EditaSimulacao = new HashMap<>();

		EditaSimulacao.put("nome", "Paulo Nascimento");
		EditaSimulacao.put("cpf", "05307431082");
		EditaSimulacao.put("email", "teste@teste.com");
		EditaSimulacao.put("valor", "5");
		EditaSimulacao.put("parcelas", "1");
		EditaSimulacao.put("seguro", "true");

		given().contentType("application/json").body(EditaSimulacao).when().put("/api/v1/simulacoes/{NUMERO_CPF}")
				.then().statusCode(400).assertThat()
				.body("erros.parcelas", equalTo("Parcelas deve ser igual ou maior que 2")).log().all();

	}

	@Test
	public void editaSimulacaoCpfNaoEncontrado() {

		Map<String, String> EditaSimulacao = new HashMap<>();

		EditaSimulacao.put("nome", "Paulo Nascimento");
		EditaSimulacao.put("cpf", "0183721811");
		EditaSimulacao.put("email", "teste@teste.com");
		EditaSimulacao.put("valor", "5");
		EditaSimulacao.put("parcelas", "1");
		EditaSimulacao.put("seguro", "true");

		given().contentType("application/json").body(EditaSimulacao).when().put("/api/v1/simulacoes/0183721811").then()
				.statusCode(404).assertThat().body("mensagem", equalTo("CPF 0183721811 não encontrado")).log().all();

	}

	@Test
	public void consultarTodasSimulacoes() {

		given().when().get("/api/v1/simulacoes/").then().statusCode(200).log().all();
	}

	@Test
	public void consultarSimulacaoPorCpf() {

		given().when().get("/api/v1/simulacoes/{NUMERO_CPF}").then().statusCode(200).log().all();
	}

	@Test
	public void consultarSimulacaoPorCpfNaoEncontrado() {

		given().when().get("/api/v1/simulacoes/78257076058").then().statusCode(404).assertThat()
				.body("mensagem", equalTo("CPF 78257076058 não encontrado")).log().all();
	}

	@Test
	public void removerSimulacao() {

		given().when().delete("/api/v1/simulacoes/{NUMERO_CPF}").then().statusCode(200).log().all();
	}

	@Test
	public void removerSimulacaoCpfNaoInformado() {

		given().when().delete("/api/v1/simulacoes/").then().statusCode(405).log().all();
	}

	@Test
	public void removerSimulacaoCpfInvalido() {

		given().when().delete("/api/v1/simulacoes/a").then().statusCode(400).log().all();
	}

}
