#language:pt 

Funcionalidade: Deletar BootStrap
Cenario: Add Customer

Dado que esteja na pagina "https://www.grocerycrud.com/v1.x/demo/bootstrap_theme"
Quando Alterar o ComboBox
E Clicar em Add Customer
E Preencher os campos 
E que esteja na pagina "https://www.grocerycrud.com/v1.x/demo/bootstrap_theme"
E Na Coluna Search Name Digitar o Name
E Clicar no Checkbox
E Clicar no Botao Delete
E Validar a Mensagem
Entao valido a mensagem de sucesso





