package steps;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;

public class StepsBootstrap {

	WebDriver driver;

	@Before

	public void before() {
		System.setProperty("webdriver.chomer.driver", "chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

	}
	
	
	/*
	@After

	public void finalizar() {
		driver.quit();
	}
	
	*/

	@Dado("^que esteja na pagina \"([^\"]*)\"$")
	public void que_esteja_na_pagina(String url) {

		driver.get(url);
		driver.manage().window().maximize();

	}

	@Quando("^Alterar o ComboBox$")
	public void alterar_o_ComboBox() {

		driver.findElement(By.id("switch-version-select")).click();
		driver.findElement(By.xpath("//*[@id=\"switch-version-select\"]/option[2]")).click();
	}

	@Quando("^Clicar em Add Customer$")
	public void clicar_em_Add_Customer() {

		driver.findElement(By.xpath("//*[@id=\"gcrud-search-form\"]/div[1]/div[1]/a")).click();

	}

	@Quando("^Preencher os campos$")
	public void preencher_os_campos() {

		driver.findElement(By.id("field-customerName")).sendKeys("Teste Sicredi");
		driver.findElement(By.id("field-contactLastName")).sendKeys("Teste");
		driver.findElement(By.id("field-contactFirstName")).sendKeys("Paulo Nascimento");
		driver.findElement(By.id("field-phone")).sendKeys("51 9999-9999");
		driver.findElement(By.id("field-addressLine1")).sendKeys("Av Assis Brasil, 3970");
		driver.findElement(By.id("field-addressLine2")).sendKeys("Torre D");
		driver.findElement(By.id("field-city")).sendKeys("Porto Alegre");
		driver.findElement(By.id("field-state")).sendKeys("RS");
		driver.findElement(By.id("field-postalCode")).sendKeys("91000-000");
		driver.findElement(By.id("field-country")).sendKeys("Brasil");
		driver.findElement(By.id("field-country")).sendKeys("Brasil");
		driver.findElement(By.xpath("//*[@id=\"field_salesRepEmployeeNumber_chosen\"]/a/span")).click();
		driver.findElement(By.xpath("//*[@id=\"field_salesRepEmployeeNumber_chosen\"]/div/div/input"))
				.sendKeys("Fixter");
		driver.findElement(By.cssSelector("#field_salesRepEmployeeNumber_chosen > a > span")).click();
		driver.findElement(By.id("field-creditLimit")).sendKeys("200");

		driver.findElement(By.id("form-button-save")).click();

	}

	@Entao("^valido a mensagem de sucesso$")
	public void valido_a_mensagem_de_sucesso() {

		String textoElement = driver.findElement(By.xpath("//*[@id=\"report-success\"]/p")).getText();
		assertEquals("Your data has been successfully stored into the database. or", textoElement);

		String textoElement2 = driver.findElement(By.xpath("//*[@id=\"report-success\"]/p/a[1]")).getText();
		assertEquals("Edit Customer", textoElement2);

	}

}
