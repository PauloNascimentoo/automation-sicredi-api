package steps;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.PendingException;
import cucumber.api.java.Before;
import cucumber.api.java.pt.Quando;

public class StepsDeletarBootstrap {

	WebDriver driver;

	@Quando("^Na Coluna Search Name Digitar o Name$")
	public void na_Coluna_Search_Name_Digitar_o_Name() {

		driver.findElement(By.xpath("//*[@id=\"gcrud-search-form\"]/div[2]/table/thead/tr[2]/td[3]")).click();
		driver.findElement(By.xpath("//*[@id=\"gcrud-search-form\"]/div[2]/table/thead/tr[2]/td[3]"))
				.sendKeys("Teste Sicredi");

	}

	@Quando("^Clicar no Checkbox$")
	public void clicar_no_Checkbox() {

		driver.findElement(By.xpath("//th[contains(., 'CreditLimit')]/following::input[@type='checkbox']")).click();

	}

	@Quando("^Clicar no Botao Delete$")
	public void clicar_no_Botao_Delete() {

		driver.findElement(By.xpath("(.//span[contains(., 'Delete')])[1]")).click();
		driver.findElement(By.xpath("(.//button[contains(., 'Delete')][@type='button'])[last()]")).click();

	}

	@Quando("^Validar a Mensagem$")
	public void validar_a_Mensagem() {

		String textoElement = driver.findElement(By.xpath("(.//button[contains(., 'Delete')][@type='button'])[last()]"))
				.getText();
		assertEquals("Your data has been successfully deleted from the database.", textoElement);

	}

}
